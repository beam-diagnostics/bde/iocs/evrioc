< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("CONTROL_GROUP",                "LAB-DEV00")
epicsEnvSet("AMC_DEVICE",                   "08:00.0")
epicsEnvSet("AMC_NAME",                     "TS-EVR-000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("EVENT_CLOCK"                   "88.0525")

#- load MTCA support
iocshLoad("../iocsh/mtca.iocsh")

###############################################################################
iocInit
###############################################################################

#- EVR img size: 3011417
#- MTCA-EVR and PCIe FPGA: Kintex7 7K70T, bits=24,090,592 page=32 and in .bit size: 3011417

#- Bit file examples
#- mTCA-EVR-300-18070207.bit
#- PCIe-EVR-300DC-17050207.bit

epicsEnvSet("BIN_SIZE", 3011417)

# flashread example
#flashread("$(DEVICE_NAME):FLASH", 0, $(BIN_SIZE), "/tmp/$(DEVICE_NAME)_bak.bin")
# flashwrite example
#flashwrite("$(DEVICE_NAME):FLASH", 0, "/tmp/$(DEVICE_NAME)_new.bin")

date
###############################################################################
