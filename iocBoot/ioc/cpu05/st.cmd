< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("CONTROL_GROUP",                "LAB-FC05")
epicsEnvSet("AMC_DEVICE",                   "0B:00.0")
epicsEnvSet("AMC_NAME",                     "TS-EVR-000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("EVENT_CLOCK"                   "88.0525")

#- load MTCA support
iocshLoad("../iocsh/mtca.iocsh")

###############################################################################
iocInit
###############################################################################

#- bugfix for EVR loosing the timestamp for 5 seconds every 7-8 hours
dbpf $(PREFIX)DC-Tgt-SP 70

#- setup EVR clock
iocshLoad("../iocsh/clock_init.iocsh", "PREFIX=$(PREFIX)")

#- setup EVR standalone mode
#- iocshLoad("../iocsh/standalone_init.iocsh", "PREFIX=$(PREFIX)")

#- setup EVR triggers
iocshLoad("../iocsh/fc_triggers_init.iocsh", "PREFIX=$(PREFIX)")

#- enable the EVR
dbpf $(PREFIX)Ena-Sel "Enabled"

date
###############################################################################
