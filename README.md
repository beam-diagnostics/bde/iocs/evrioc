# superioc

Define `PROD_NAME` in `CONFIG.local` file in the TOP folder of this project; see `EXAMPLE_CONFIG_local`.
It will be included by the `superApp/src/Makefile` and used to name the IOC application binary file.

